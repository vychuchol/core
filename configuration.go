package core

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
)

// Configuration represents loaded configuration file.
type Configuration map[string]interface{}

// LoadConfiguration loads configuration from specified file.
func LoadConfiguration(filename string) (Configuration, error) {
	c := make(Configuration)
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, fmt.Errorf("can not read configuration file: %v", err)
	}
	if err := json.Unmarshal(data, &c); err != nil {
		return nil, fmt.Errorf("can not parse configuration file: %v", err)
	}
	return c, nil
}

func convertInt(m map[string]interface{}, key string) (int, bool) {
	if m == nil {
		return 0, false
	}
	if f, ok := m[key].(float64); ok {
		if f == math.Trunc(f) {
			return int(f), true
		}
	}
	return 0, false
}

func convertIntOr(m map[string]interface{}, key string, def int) int {
	if v, ok := convertInt(m, key); ok {
		return v
	}
	return def
}

func convertString(m map[string]interface{}, key string) (string, bool) {
	if m == nil {
		return "", false
	}
	v, ok := m[key].(string)
	return v, ok
}

func convertStringOr(m map[string]interface{}, key, def string) string {
	if v, ok := convertString(m, key); ok {
		return v
	}
	return def
}

func (c Configuration) port(def int) int {
	if port, ok := convertInt(c, "port"); ok {
		if port > 0 && port < 65536 {
			return port
		}
	}
	return def
}

// Port get network port from configuration.
// If port is not valid integer number in range 1 to 65535, returns 80.
func (c Configuration) Port() int {
	return c.port(80)
}

// DatabaseConnectionString get database connection string from configuration.
// Parameters are in object "database".
// See DatabaseParameters for more detail about default values.
func (c Configuration) DatabaseConnectionString() string {
	dbname, host, port, user, password := c.DatabaseParameters()
	return fmt.Sprintf("user=%s dbname=%s password=%s host=%s port=%d sslmode=disable",
		user,
		dbname,
		password,
		host,
		port,
	)
}

// DatabaseParameters get database parameters from configuration.
// Default parameters are:
// "dbname":   "database"
// "host":     "localhost"
// "port":     5432
// "user":     "postgres"
// "password": "postgres"
func (c Configuration) DatabaseParameters() (dbname, host string, port int, user, password string) {
	dbc, _ := c["database"].(map[string]interface{})
	return convertStringOr(dbc, "dbname", "database"),
		convertStringOr(dbc, "host", "localhost"),
		Configuration(dbc).port(5432),
		convertStringOr(dbc, "user", "postgres"),
		convertStringOr(dbc, "password", "postgres")
}

// SessionsConfiguration return key and secret from configuration.
// Parameters are in object "sessions".
// Default parameters are:
// "name":       "vychuchol"
// "key":        "vychuchol"
// "secret":     random generated Base64 string length 32 characters
// "expiration": 24*60*60 (1 day in seconds)
func (c Configuration) SessionsConfiguration() (name, key, secret string, expiration int) {
	sc, _ := c["sessions"].(map[string]interface{})
	name = convertStringOr(sc, "name", "vychuchol")
	key = convertStringOr(sc, "key", "vychuchol")
	ok := false
	secret, ok = convertString(sc, "secret")
	expiration = convertIntOr(sc, "expiration", 24*60*60)
	if !ok {
		bytes := make([]byte, 24)
		rand.Read(bytes)
		secret = base64.StdEncoding.EncodeToString(bytes)
	}
	return
}
