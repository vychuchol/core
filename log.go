package core

import (
	"fmt"
	"log"
	"os"
)

// RedirectLog create log file for append and redirects log output to them.
func RedirectLog(filename string) *os.File {
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Println(fmt.Errorf("can not create log file for append; %v", err))
		return nil
	}
	log.SetOutput(file)
	return file
}
