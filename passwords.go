package core

import (
	"crypto/sha256"
	"encoding/hex"
)

func HashPassword(password, salt string) string {
	hash := sha256.Sum256([]byte(password + salt))
	return hex.EncodeToString(hash[:])
}
