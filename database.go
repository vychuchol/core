package core

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq" // PostgreSQL driver.
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/postgres"
	_ "github.com/golang-migrate/migrate/source/file" // Migration driver for files.
)

// OpenDatabase open database with specified connection string.
func OpenDatabase(c string) (*sql.DB, error) {
	return sql.Open("postgres", c)
}

func createMigration(db *sql.DB, path string, version int) (*migrate.Migrate, error) {
	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return nil, err
	}
	m, err := migrate.NewWithDatabaseInstance("file://"+path, "postgres", driver)
	if err != nil {
		return nil, err
	}
	return m, nil
}

// CheckDatabaseSchemaVersion check if schema has specified version.
func CheckDatabaseSchemaVersion(db *sql.DB, path string, version int) error {
	m, err := createMigration(db, path, version)
	if err != nil {
		return err
	}
	v, _, err := m.Version()
	if err != nil && err != migrate.ErrNilVersion {
		return err
	}
	if int(v) != version {
		return fmt.Errorf("expected version schema %d but current schema has version %d", version, v)
	}
	return nil
}

// MigrateDatabase set up schema with specified version.
func MigrateDatabase(db *sql.DB, path string, version int) error {
	m, err := createMigration(db, path, version)
	if err != nil {
		return err
	}
	return m.Migrate(uint(version))
}
