package core

import (
	"html/template"
	"path/filepath"
)

// LoadTemplates load all *.tpl files from specified directory.
// Templates can contains functions calls from functions.
// If problem will raised, function panics.
func LoadTemplates(directory string, functions template.FuncMap) *template.Template {
	return template.Must(template.New("main").Funcs(functions).ParseGlob(filepath.Join(directory, "*.tpl")))
}
