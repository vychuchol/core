package core

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

// Handler process HTTP requests.
type Handler func(http.ResponseWriter, *http.Request)

type route struct {
	rule    string
	handler Handler
}

// Web represents running instance on specific port.
// URLs can be routed and files from directory /static are automatically distributed.
type Web struct {
	port                int
	server              *http.Server
	staticRouteHandler  http.Handler
	routes              []route
	defaultRouteHandler Handler
}

// NewWeb create web with specified port.
// Port must be between 1 and 65535, otherwise 80 is chosen.
func NewWeb(port int) *Web {
	if port < 1 || port > 65535 {
		port = 80
	}

	w := &Web{}
	w.port = port
	w.server = &http.Server{
		Addr:    fmt.Sprintf(":%d", port),
		Handler: w,
	}
	w.staticRouteHandler = http.FileServer(http.Dir("."))
	return w
}

// Port returns network port, where Web is listening for http requests.
func (w *Web) Port() int {
	return w.port
}

func (w *Web) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if strings.HasPrefix(req.URL.Path, "/static/") {
		w.staticRouteHandler.ServeHTTP(rw, req)
		return
	}
	for _, route := range w.routes {
		if strings.HasPrefix(req.URL.Path, route.rule) {
			route.handler(rw, req)
			return
		}
	}
	if w.defaultRouteHandler != nil {
		w.defaultRouteHandler(rw, req)
	}
}

// Route add new router rule.
func (w *Web) Route(rule string, h Handler) bool {
	if h == nil {
		return false
	}
	w.routes = append(w.routes, route{rule: rule, handler: h})
	return true
}

// DefaultRoute set handler for all request with pass no other route.
func (w *Web) DefaultRoute(h Handler) {
	w.defaultRouteHandler = h
}

// Start web server.
// This blocks goroutine.
func (w *Web) Start() error {
	err := w.server.ListenAndServe()
	log.Println(err)
	return err
}
