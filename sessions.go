package core

import (
	"fmt"
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
)

// Sessions represents session mechanism based on own key and secret.
// Sessions are concurrent-safe, based on JWT.
type Sessions struct {
	name       string
	key        string
	secret     string
	expiration int
}

// NewSessions create new Session.
func NewSessions(name, key, secret string, expiration int) *Sessions {
	return &Sessions{name: name, key: key, secret: secret, expiration: expiration}
}

// Load session from HTTP request.
func (s *Sessions) Load(req *http.Request) (map[string]interface{}, error) {
	c, err := req.Cookie(s.name)
	if err != nil {
		return nil, fmt.Errorf("cookie with session is not set: %v", err)
	}
	token, err := jwt.Parse(c.Value, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(s.secret), nil
	})
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if data, ok := claims[s.key]; ok {
			if m, ok := data.(map[string]interface{}); ok {
				return m, nil
			}
			return nil, fmt.Errorf("session data has wrong type")
		}
		return nil, fmt.Errorf("session data not found")
	}
	return nil, fmt.Errorf("session is not valid: %v", err)
}

// LoadAndRestore load session from HTTP request and restore expiration time.
func (s *Sessions) LoadAndRestore(rw http.ResponseWriter, req *http.Request) (map[string]interface{}, error) {
	if err := s.Restore(rw, req); err != nil {
		return nil, err
	}
	return s.Load(req)
}

// Save session to header field "Authorization" of HTTP response.
func (s *Sessions) Save(rw http.ResponseWriter, data map[string]interface{}) error {
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{s.key: data}).SignedString([]byte(s.secret))
	if err != nil {
		return fmt.Errorf("can not save session: %v", err)
	}
	http.SetCookie(rw, &http.Cookie{
		Name:   s.name,
		Value:  token,
		MaxAge: s.expiration,
	})
	return nil
}

// Restore session expiration time.
func (s *Sessions) Restore(rw http.ResponseWriter, req *http.Request) error {
	c, err := req.Cookie(s.name)
	if err != nil {
		return fmt.Errorf("cookie with session is not set: %v", err)
	}
	c.MaxAge = s.expiration
	http.SetCookie(rw, c)
	return nil
}

// Delete session.
func (s *Sessions) Delete(rw http.ResponseWriter) error {
	http.SetCookie(rw, &http.Cookie{
		Name:   s.name,
		Value:  "",
		MaxAge: -1,
	})
	return nil
}
